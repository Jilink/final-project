const express = require('express');
const router = express.Router();
const models = require('./../models')

// fetch all
router.get('/', async function(req, res) {
  try{
    const [rows, fields] = await models.article.fetchAll();
    res.format({
      "text/html" : function () {
        res.render("article/index", {
          articles : rows
        })
      },
      "application/json" : function () {
        res.json({
          articles : rows
          })
      }
    })
  } catch (error){
  }
  
});



// fetch one

router.post('/articleId', async function(req, res, next) { // for the search bar
  try{
    res.redirect('http://localhost:3000/articles/'+req.body.articleId)

  } catch (error){
    res.json(error)
  }
  // next()
});

router.get('/:articleId', async function(req, res, next) {
  try{
    const [rows, fields] = await models.article.fetchOne(req.params.articleId);
    console.log(rows)
    res.format({
      "text/html" : function () {
        res.render("article/index", {
          articles : rows
        })
      },
      "application/json" : function () {
        res.json({
        article : rows
        })
        
      }
    })
  } catch (error){
  }
  next()
});


// add

router.post('/', async function(req, res, next) {
  try{
    await models.article.add(req.body);
    res.redirect('http://localhost:3000/articles/')

  } catch (error){
  }
  next()
  
});

router.get('/add', async function(req, res) { // vers le forumlaire pour add
  try{
    res.format({
      "text/html" : function () {
        res.render("article/add", {})
      },
      "application/json" : function () {
        res.json(rows)
      }
    })
  } catch (error){
  }
  
});


// delete

router.delete('/:articleId', async function(req, res,next) {
  try {
    await models.article.delete(req.params.articleId);
    res.redirect('http://localhost:3000/articles/')
  } catch (error){
    res.json(error)
  }  
  // next()
});


//mofify

router.put('/:id', async function(req, res) {
  try{
    console.log(req.body)
    await models.article.modify(req.params.id, req.body);
    res.redirect('http://localhost:3000/articles/')

  } catch (error){
    res.json(error)
  }  
  // next()
});

router.get('/:id/edit', async function(req, res) { // page de formulaire pour modifier
  try {
    const [rows, fields] = await models.article.fetchOne(req.params.id);
    res.format({
      "text/html" : function () {
        res.render("article/edit", {
          article : rows[0]
        })
      }
    })
  } catch (error){
    res.json(error)
  }
  
});

//Router not found 

router.use((req,res)=> {
  res.status(404)
  res.end('Not found')
})

module.exports = router
