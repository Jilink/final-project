module.exports = {
  fetchAll() {
    return connection.execute('SELECT * FROM inventories')
  },
  fetchOne(id) {
    return connection.execute(`SELECT a.article_id, a.name, l.quantity, a.price FROM articles a, inventories_articles l, inventories i
    WHERE  a.article_id = l.article_id and l.inventory_id = ${id};`)

  },
  delete(id) {
    return connection.execute(`DELETE FROM inventories_articles WHERE article_id = ${id};`)
  },
  add(inventory) {
    return connection.execute(`INSERT INTO inventories_articles VALUES  (${inventory.id},1, ${inventory.quantity});`)

  },
//   modify(id, column, value) {
//     return connection.execute(`UPDATE users SET ${column} = '${value}' WHERE user_id = ${id};`)
//   },
} 