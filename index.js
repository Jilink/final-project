

(async () => {
  const config = require ("./config.json")
  const express = require('express');
  const app = express();  
  const mysql = require('mysql2/promise');
  const methodOverride = require('method-override')
  const bodyParser = require('body-parser')
  const cors = require("cors")
  


  app.use(cors())
  app.set('views', './views')
  app.set('view engine', 'pug')
  app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
  app.use(express.static('public'))
  app.use(methodOverride('_method'))
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({

    extended: true
  }))


  global.connection = await mysql.createConnection(config)

  const user = require('./controllers/index').user;
  app.use('/users', user);
  const inventory = require('./controllers/index').inventory;
  app.use('/inventories', inventory);

  const article = require('./controllers/index').article
  app.use('/articles', article)

// Admin connection page
  app.get('/', async function(req, res, next) {
    try{
      res.format({
        "text/html" : function () {
          res.render("home/connect", {
          })
        },
        "application/json" : function () {
          res.json( {
          }
            )
        }
      })
    } catch (error){
    }
  });

  // if successfully connected
  app.post('/', async function(req, res, next) {
    try{
      res.format({
        "text/html" : function () {
          res.render("home/index", {
            username : req.body.username
          })
        },
        "application/json" : function () {
          res.json( {
          }
            )
        }
      })
    } catch (error){
    }

  
  });


  app.listen(3000)
  console.log('Listening ...')
})()

