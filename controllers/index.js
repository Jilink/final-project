module.exports = {
  user: require('./user'),
  article: require('./article'),
  inventory: require('./inventory'),
}
