const express = require('express');
const router = express.Router();
const models = require('./../models')

// fetch all
router.get('/', async function(req, res, next) {
  try{
    const [rows, fields] = await models.user.fetchAll();
    res.format({
      "text/html" : function () {
        res.render("user/index", {
          users : rows
        })
      },
      "application/json" : function () {
        res.json( {
          users : rows
        }
          )
      }
    })
  } catch (error){
  }

  next()
  
});

//fetch one

router.post('/byId', async function(req, res, next) { // pour le formulaire avec la case id
  try{
    res.redirect('http://localhost:3000/users/'+req.body.ID)

  } catch (error){
    res.json(error)
  }
  
  next()

});


router.get('/:userid', async function(req, res, next) {
  try{
    const [rows, fields] = await models.user.fetchOne(req.params.userid);
    res.format({
      "text/html" : function () {
        res.render("user/index", {
          users : rows
        })
      },
      "application/json" : function () {
        res.json({
          user : rows[0]
        })
      }
    })
  } catch (error){
  }
  
  next()

});

// delete

router.delete('/:id', async function(req, res,next) {
  try {
    await models.user.delete(req.params.id);
    res.redirect('http://localhost:3000/users/')

  } catch (error){
    res.json(error)
  }  
  next()

});



// add

router.post('/', async function(req, res, next) {
  try{
    await models.user.add(req.body);
    res.redirect('http://localhost:3000/users/')

  } catch (error){
  }
  next()
  
});

router.get('/add', async function(req, res) { // vers le forumlaire pour add
  try{
    res.format({
      "text/html" : function () {
        res.render("user/add", {})
      },
      "application/json" : function () {
        res.json(rows)
      }
    })
  } catch (error){
  }
  
});


// modify

router.put('/:id', async function(req, res) {
  try{
    console.log(req.body)
    await models.user.modify(req.params.id, req.body);
    res.redirect('http://localhost:3000/users/')

  } catch (error){

  }
  
});

router.get('/:id/edit', async function(req, res) { // page de formulaire pour modifier
  try {
    const [rows, fields] = await models.user.fetchOne(req.params.id);
    res.format({
      "text/html" : function () {
        res.render("user/edit", {
          user : rows[0]
        })
      }
    })
  } catch (error){
    res.json(error)
  }
  
});






module.exports = router
